<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\DepartemenController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\UserController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {

    Route::prefix('user')->group(function() {
        Route::get('/lists/{type}', [UserController::class, 'userLists'])->name('user.lists');
        Route::get('/{type}', [UserController::class, 'index'])->name('user.index');
        Route::delete('/{id}/{type}', [UserController::class, 'removePegawai'])->name('user.remove');
        Route::post('/exec', [UserController::class, 'execFormUser'])->name('user.exec');
    });

    Route::prefix('member')->group(function() {
        Route::get('/{user}/{tipe}', [UserController::class, 'changeMemberType'])->name('member.changetype');
        Route::get('/{departemen}', [DepartemenController::class, 'memberLists'])->name('member.departemen');
    });

    Route::prefix('departemen')->group(function() {
        Route::get('/form/{id?}', [DepartemenController::class, 'form'])->name('departemen.form');
        Route::get('/lists', [DepartemenController::class, 'departemenLists'])->name('departemen.lists');
        Route::get('/', [DepartemenController::class, 'index'])->name('departemen.index');
        Route::post('/post', [DepartemenController::class, 'post'])->name('departemen.post');
        Route::delete('/{id}', [DepartemenController::class, 'delete'])->name('departemen.remove');
    });

    Route::prefix('kategori')->group(function() {
        Route::get('/lists', [CategoryController::class, 'lists'])->name('category.lists');
        Route::get('/', [CategoryController::class, 'index'])->name('category.index');
        Route::post('/post', [CategoryController::class, 'post'])->name('category.post');
        Route::delete('/{id}', [CategoryController::class, 'delete'])->name('category.remove');
    });

    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';
