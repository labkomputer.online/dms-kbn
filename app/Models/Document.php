<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Document extends Model
{
    use HasFactory, HasUuids, SoftDeletes, Sluggable;

    protected $keyType = 'string';
    public $incrementing = false;

    protected $fillable = [
        'title',
        'slug',
        'category',
        'description',
        'tags',
        'departemen',
        'status_type',
        'status_at',
        'status_by'
    ];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function categoryinfo()
    {
        return $this->belongsTo(Category::class, 'category');
    }

    public function departemeninfo()
    {
        return $this->belongsTo(Departemen::class, 'departemen');
    }

    public function executor()
    {
        return $this->belongsTo(User::class, 'status_by');
    }
}
