<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Inertia\Response;

class CategoryController extends Controller
{

    public function index(): Response
    {
        $cats = Category::query()->orderBy("name", "asc")->get();
        return Inertia::render("Category/CategoryMain", [
            "kategori" => $cats
        ]);
    }

    public function lists(Request $r)
    {
        $q = Category::query()->with(['catparent']);

        if ($s = $r->query('s')) {
            $q->where('name', 'like', '%'.$s.'%');
        }

        $res = $q->paginate(10);

        return response()->json([
            "status" => "success",
            "message" => "ready",
            "data" => $res
        ]);
    }

    public function post(Request $r)
    {
        try {
            $data = [
                "name" => $r->json("name"),
                "info" => $r->json("info")
            ];

            if ($r->json("parentSel")["value"] != "") {
                $data["parent"] = $r->json("parentSel")["value"];
            }

            if ($r->json("id") != "") {
                Category::where("id", $r->json("id"))->update($data);
                return to_route("category.index")->with("status", "success")->with("message", "Data berhasil diperbarui");
            } else {
                Category::create($data);
            }
            return to_route("category.index")->with("status", "success")->with("message", "Data berhasil dibuat");
        } catch (\Throwable $th) {
            throw $th;
            return to_route("category.index")->with("status", "fail")->with("message", "Gagal terhubung");
        }
    }

    public function remove($id)
    {
        try {
            $res = Category::where('id', $id)->first();
            if (!$res) {
                return to_route("category.index")->with("status", "notfound")->with("message", "Data tidak ditemukan");
            }
            Category::where('id', $id)->delete();
            return to_route("category.index")->with("status", "success")->with("message", "Data sudah dihapus");
        } catch (\Throwable $th) {
            //throw $th;
            return to_route("category.index")->with("status", "fail")->with("message", "Gagal menghubungkan jaringan");
        }
    }

}
