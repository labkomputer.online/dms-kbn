<?php

namespace App\Http\Controllers;

use App\Models\Departemen;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Inertia\Inertia;
use Inertia\Response;

class UserController extends Controller
{

    public function index($type): Response
    {
        $dep = Departemen::all();
        return Inertia("User/UserMain", [
            "type" => $type,
            "departemen" => $dep
        ]);
    }

    public function userLists(Request $r, $type)
    {
        $q = User::query()->with("departemennya");
        if ($type == "admin") {
            $q->where("role", "!=", "employee")->where("role", "!=", "root");
        } else {
            $q->where("role", "employee");
        }

        if ($s = $r->query('s')) {
            $q->where('name', 'like', '%'.$s.'%')
            ->where('role', 'like', '%'.$s.'%');
        }
        $res = $q->orderBy("name", "asc")->paginate(10);

        return response()->json([
            "status" => "success",
            "message" => "ready",
            "data" => $res
        ]);
    }

    public function execFormUser(Request $r)
    {
        try {
            $data = [
                "name" => $r->json("name"),
                "email" => $r->json("email"),
                "role" => $r->json("role")["value"]
            ];

            if ($r->json("role")["value"] == "employee") {
                $data["departemen"] = $r->json("departemen")["value"];
                $data["jabatandep"] = $r->json("jabatan")["value"];
            }

            if ($r->json("id") == "") {
                $data["password"] = Hash::make($r->json("pswd"));
                User::create($data);
            } else {
                if ($r->json("pswd") != "") {
                    $data["password"] = Hash::make($r->json("pswd"));
                }
                User::where("id", $r->json("id"))->update($data);
            }

            return to_route("user.index", [
                "type" => $r->json("role")["value"]
            ])->with("status", "success")->with("message", $r->id == "" ? "Berhasil menambahkan data pegawai" : "Berhasil memperbarui data");
        } catch (\Throwable $th) {
            throw $th;
            return to_route("user.index",  [
                "type" => $r->json("role")["value"]
            ])->with("status", "fail")->with("message", "Gagal menghubungkan data");
        }
    }

    public function removePegawai($id, $type)
    {
        $q = User::where("id", $id)->first();
        if (!$q) {
            return response()->json([
                "status" => "notfound",
                "message" => "Data tidak ditemukan",
                "data" => null
            ], 404);
        }

        User::where("id", $id)->delete();
        return to_route("user.index", [
            "type" => $type
        ])
            ->with("status", "success")
            ->with("message", "Berhasil menghapus data");
    }

    public function changeMemberType($user, $tipe)
    {
        try {
            $q = User::query()->where("id", $user)->update([
                "jabatandep" => $tipe
            ]);

            return response([
                'status' => 'success',
                'data' => null,
                'message' => 'Berhasil diperbarui'
            ]);
        } catch (\Throwable $th) {
            //throw $th;
            return response([
                'status' => 'failed',
                'data' => null,
                'message' => 'Gagal diperbarui'
            ], 500);
        }
    }
}
