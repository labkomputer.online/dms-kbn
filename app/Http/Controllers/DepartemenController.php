<?php

namespace App\Http\Controllers;

use App\Models\Departemen;
use App\Models\User;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Inertia\Response;

class DepartemenController extends Controller
{

    public function index(Request $r): Response
    {
        return Inertia::render("Departemen/DepartemenMain");
    }

    public function form(Request $r, $id = ""): Response
    {
        $departemen = null;

        if ($id != "") {
            $departemen = Departemen::where("id", $id)->first();
        }

        return Inertia::render("Departemen/DepartemenForm", [
            "departemen" => $departemen
        ]);
    }

    public function departemenLists(Request $r)
    {
        $q = Departemen::query();

        if ($s = $r->query('s')) {
            $q->where('name', 'like', '%'.$s.'%');
        }

        $res = $q->paginate(10);

        return response()->json([
            "status" => "success",
            "message" => "ready",
            "data" => $res
        ]);
    }

    public function memberLists($departemen)
    {
        $q = User::query()->where('departemen', $departemen)->get();

        return response([
            'status' => 'success',
            'data' => $q,
            'message' => 'ok'
        ]);
    }

    public function post(Request $r)
    {
        try {
            $data = [
                "name" => $r->json("name"),
                "info" => $r->json("info")
            ];

            if ($r->json("id") != "") {
                Departemen::where("id", $r->json("id"))->update($data);
            } else {
                Departemen::create($data);
            }

            return to_route("departemen.index")->with("status", "success")->with("message", $r->id == "" ? "Berhasil menambahkan data pegawai" : "Berhasil memperbarui data");
        } catch (\Throwable $th) {
            throw $th;
            return to_route("departemen.index")->with("status", "fail")->with("message", "Gagal memperbarui data");
        }
    }

    public function remove($id)
    {
        try {
            \DB::beginTransaction();
            Departemen::where("id", $id)->delete();
            User::where("departemen", $id)->update([
                "departemen" => null,
                "jabatandep" => null
            ]);
            \DB::commit();
            return to_route("departemen.index")->with("status", "success")->with("message", "Berhasil memperbarui data");
        } catch (\Throwable $th) {
            \DB::rollback();
            throw $th;
            return to_route("departemen.index")->with("status", "fail")->with("message", "Gagal memperbarui data");
        }
    }

}
