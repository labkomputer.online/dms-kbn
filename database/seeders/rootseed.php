<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class rootseed extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::factory()->create([
            'name' => 'sysroot',
            'email' => 'sys.root@lab.email',
            'usercode' => 'sysroot',
            'role' => 'root',
            'email_verified_at' => now(),
            'password' => Hash::make('ZAQ*000#poi@')
        ]);
    }
}
