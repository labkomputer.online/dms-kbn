<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('title');
            $table->text('slug');
            $table->string('category');
            $table->text('description')->nullable();
            $table->text('tags')->nullable();
            $table->string('status_type')->default('pending');
            $table->timestamp('status_at')->nullable();
            $table->string('status_by')->nullable();
            $table->string('accfile')->nullable();
            $table->string('setpublic')->nullable('n');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('documents');
    }
};
