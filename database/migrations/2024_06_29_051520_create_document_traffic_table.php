<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('document_traffics', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('fileid');
            $table->string('line');
            $table->string('user')->nullable();
            $table->string('clientip');
            $table->string('city')->nullable();
            $table->string('region')->nullable();
            $table->string('timezone')->nullable();
            $table->string('ispname')->nullable();
            $table->string('isporg')->nullable();
            $table->string('devicename')->nullable();
            $table->string('devicetype')->nullable();
            $table->string('browser')->nullable();
            $table->string('useragent')->nullable();
            $table->string('os')->nullable();
            $table->string('downloadfile')->default('n');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('document_traffic');
    }
};
