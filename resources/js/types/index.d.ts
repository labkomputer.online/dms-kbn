import { Config } from 'ziggy-js';

export interface User {
    id: string;
    name: string;
    email: string;
    email_verified_at?: string;
    role?: string;
    departemen?: string;
    jabatandep?: string;
    departemennya?: Departemen;
}

export interface Departemen {
    id?: string;
    name?: string;
    info?: string;
    created_at?: Date;
    pegawainya?: User[];
}

export interface Category {
    id?: string;
    name?: string;
    slug?: string;
    info?: string;
    parent?: string;
    catchild?: Category[];
    catparent?: Category;
}

export type PageProps<T extends Record<string, unknown> = Record<string, unknown>> = T & {
    auth?: {
        user?: User;
    };
    ziggy?: Config & { location: string };
    departemen?: Departemen;
    anggota?: User[];
};
