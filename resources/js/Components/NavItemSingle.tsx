import React, { PropsWithChildren } from 'react'
import NavLink from './NavLink'
import { PageProps } from '@/types';
import { usePage } from '@inertiajs/react';

function NavItemSingle({
    menuPath,
    to,
    currentMenu,
    buttonSVG,
    label,
    roles,
    buttonClick,
}: PropsWithChildren<{
    menuPath?: string[];
    to: string;
    currentMenu: string;
    roles?: string[];
    buttonSVG: React.ReactNode;
    label: string;
    buttonClick?: () => void;
}>) {
    const pp: PageProps = usePage().props;

    if (roles) {
        if (!roles.includes(pp?.auth?.user?.role as string)) {
            return <React.Fragment></React.Fragment>
        }
    }

    return (
        <li className="nav-item">
            <NavLink
                active={menuPath?.includes(currentMenu) ? true : false}
                href={to}
                className="group"
                onClick={() => buttonClick?.()}
            >
                <div className="flex items-center">
                    {buttonSVG}
                    <span className="ltr:pl-3 rtl:pr-3 text-black dark:text-[#506690] dark:group-hover:text-white">
                        {label}
                    </span>
                </div>
            </NavLink>
        </li>
    )
}

export default NavItemSingle
