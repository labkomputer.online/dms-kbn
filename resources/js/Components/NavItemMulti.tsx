import React, { PropsWithChildren } from "react";
import PanahMenu from "./svg/PanahMenu";
import AnimateHeight from "react-animate-height";
import { usePage } from "@inertiajs/react";
import { PageProps } from "@/types";

function NavItemMulti({
    menuPath,
    currentMenu,
    buttonSVG,
    label,
    roles,
    buttonClick,
    children,
}: PropsWithChildren<{
    menuPath: string[];
    currentMenu: string;
    roles?: string[];
    buttonSVG: React.ReactNode;
    label: string;
    buttonClick?: () => void;
}>) {
    const pp: PageProps = usePage().props;

    if (roles) {
        if (!roles.includes(pp?.auth?.user?.role as string)) {
            return <React.Fragment></React.Fragment>
        }
    }

    return (
        <li className="menu nav-item">
            <button
                style={{fontSize: '0.875rem', fontWeight: 'normal'}}
                type="button"
                className={`${
                    menuPath.includes(currentMenu) ? "active" : ""
                } nav-link group w-full`}
                onClick={() => buttonClick?.()}
            >
                <div className="flex items-center">
                    {buttonSVG}
                    <span className="ltr:pl-3 rtl:pr-3 text-black dark:text-[#506690] dark:group-hover:text-white">
                        {label}
                    </span>
                </div>
                <div
                    className={
                        menuPath.includes(currentMenu)
                            ? "rotate-90"
                            : "rtl:rotate-180"
                    }
                >
                    <PanahMenu />
                </div>
            </button>
            <AnimateHeight
                duration={300}
                height={
                    menuPath.includes(currentMenu) ? "auto" : 0
                }
            >
                <ul className="sub-menu text-gray-500">{children}</ul>
            </AnimateHeight>
        </li>
    );
}

export default NavItemMulti;
