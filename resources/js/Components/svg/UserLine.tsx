import React from 'react'

function UserLine(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg {...props}>
      <circle cx="12" cy="6" r="4" stroke="currentColor" strokeWidth="1.5" />
      <ellipse opacity="0.5" cx="12" cy="17" rx="7" ry="4" stroke="currentColor" strokeWidth="1.5" />
    </svg>
  )
}

export default UserLine