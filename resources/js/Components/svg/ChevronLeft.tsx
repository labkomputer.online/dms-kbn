import React from 'react'

function ChevronLeft(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg {...props}>
        <path d="M15 5L9 12L15 19" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path>
    </svg>
  )
}

export default ChevronLeft