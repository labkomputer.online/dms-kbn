const LKFoot = () => {
    return (
        <div>
            <p className="dark:text-white-dark text-center ltr:sm:text-left rtl:sm:text-right pt-6">
                © {new Date().getFullYear()}. Powered by <a href="https://jagatkreasi.digital/" target="_blank">KBN</a> v{import.meta.env['VITE_APP_VERSION']} All rights reserved.
            </p>
        </div>
    );
  };

  export default LKFoot;
