import { SidebarControl } from "@/LabState";
import { PageProps } from "@/types";
import { Link, usePage } from "@inertiajs/react";
import React from "react";
import { useRecoilState } from "recoil";
import PerfectScrollbar from "react-perfect-scrollbar";
import NavLink from "@/Components/NavLink";
import HomeSVG from "@/Components/svg/HomeSVG";
import NoteList from "@/Components/svg/NoteList";
import PanahMenu from "@/Components/svg/PanahMenu";
import AnimateHeight from "react-animate-height";
import NavItemMulti from "@/Components/NavItemMulti";
import NavItemSingle from "@/Components/NavItemSingle";
import UsersSVG from "@/Components/svg/UsersSVG";
import Kubus from "@/Components/svg/Kubus";
import PriceTagBD from "@/Components/svg/BoldDuo/PriceTagBD";
import Notes from "@/Components/svg/Notes";
import DocumentsBD from "@/Components/svg/BoldDuo/DocumentsBD";
// import { NavLink } from "react-router-dom";

function LKSide() {
    const [sidebar, setsidebar] = useRecoilState(SidebarControl);
    const pp = usePage<PageProps>();
    const toggleSidebar = () => setsidebar(!sidebar);
    const [currentMenu, setCurrentMenu] = React.useState<string>("");

    const toggleMenu = (value: string) => {
        setCurrentMenu((oldValue) => {
            return oldValue === value ? "" : value;
        });
    };

    React.useEffect(() => {
        const selector = document.querySelector(
            '.sidebar ul a[href="' + window.location.pathname + '"]'
        );
        if (selector) {
            selector.classList.add("active");
            const ul: any = selector.closest("ul.sub-menu");
            if (ul) {
                let ele: any =
                    ul.closest("li.menu").querySelectorAll(".nav-link") || [];
                if (ele.length) {
                    ele = ele[0];
                    setTimeout(() => {
                        ele.click();
                    });
                }
            }
        }
    }, []);

    React.useEffect(() => {
        if (window.innerWidth < 1024 && sidebar) {
            toggleSidebar();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [location]);

    return (
        <div className="">
            <nav
                className={`sidebar fixed min-h-screen h-full top-0 bottom-0 w-[260px] shadow-[5px_0_25px_0_rgba(94,92,154,0.1)] z-50 transition-all duration-300 text-white-dark`}
            >
                <div className=" bg-white dark:bg-black h-full">
                    <div className="flex justify-between items-center px-4 py-3">
                        <Link
                            href="/"
                            className="main-logo flex items-center shrink-0"
                        >
                            <img
                                className="w-8 ml-[5px] flex-none"
                                src="/assets/logo/kbndoc-trans.png"
                                alt="logo"
                            />
                            <span className="text-2xl ltr:ml-1.5 rtl:mr-1.5 font-semibold align-middle lg:inline dark:text-white">
                                KBNDOC
                            </span>
                        </Link>

                        <button
                            type="button"
                            className="collapse-icon w-8 h-8 rounded-full flex items-center hover:bg-gray-500/10 dark:hover:bg-dark-light/10 dark:text-white transition duration-300 rtl:rotate-180"
                            onClick={() => toggleSidebar()}
                        >
                            <svg
                                width="24"
                                height="24"
                                viewBox="0 0 24 24"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                                className="w-5 h-5 m-auto"
                            >
                                <path
                                    d="M13 19L7 12L13 5"
                                    stroke="currentColor"
                                    strokeWidth="1.5"
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                />
                                <path
                                    opacity="0.5"
                                    d="M16.9998 19L10.9998 12L16.9998 5"
                                    stroke="currentColor"
                                    strokeWidth="1.5"
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                />
                            </svg>
                        </button>
                    </div>
                    <PerfectScrollbar className="h-[calc(100vh-80px)] relative">
                        <ul className="relative font-semibold space-y-0.5 p-4 py-0">
                            <NavItemSingle
                                label="Dashboard"
                                to="/dashboard"
                                menuPath={["dashboard"]}
                                currentMenu={currentMenu}
                                buttonSVG={<HomeSVG
                                    className="group-hover:!text-primary"
                                    width="20"
                                    height="20"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                />}
                                buttonClick={() => toggleMenu("dashboard")}
                            />

                            <NavItemSingle
                                roles={["root", "admin"]}
                                label="Departemen"
                                to="/departemen"
                                menuPath={["departemen"]}
                                currentMenu={currentMenu}
                                buttonSVG={<Kubus
                                    className="group-hover:!text-primary"
                                    width="20"
                                    height="20"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                />}
                                buttonClick={() => toggleMenu("departemen")}
                            />

                            <NavItemMulti
                                roles={["root", "admin"]}
                                menuPath={["user", "user.admin", "user.pegawai"]}
                                label="Pengguna"
                                buttonSVG={
                                    <UsersSVG
                                        className="group-hover:!text-primary"
                                        width="20"
                                        height="20"
                                        viewBox="0 0 24 24"
                                        fill="none"
                                        xmlns="http://www.w3.org/2000/svg"
                                    />
                                }
                                currentMenu={currentMenu}
                                buttonClick={() => toggleMenu("user")}
                            >
                                <li>
                                    <NavLink
                                        active={
                                            currentMenu === "user.admin"
                                                ? true
                                                : false
                                        }
                                        href="/user/admin"
                                        onClick={() => toggleMenu("user.admin")}
                                    >
                                        Administrator
                                    </NavLink>
                                </li>
                                <li>
                                    <NavLink
                                        active={
                                            currentMenu === "user.pegawai"
                                                ? true
                                                : false
                                        }
                                        href="/user/pegawai"
                                        onClick={() =>
                                            toggleMenu("user.pegawai")
                                        }
                                    >
                                        Pegawai
                                    </NavLink>
                                </li>
                            </NavItemMulti>

                            <NavItemSingle
                                roles={["root", "admin"]}
                                label="Kategori"
                                to="/kategori"
                                menuPath={["kategori"]}
                                currentMenu={currentMenu}
                                buttonSVG={<PriceTagBD
                                    className="group-hover:!text-primary"
                                    width="20"
                                    height="20"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                />}
                                buttonClick={() => toggleMenu("kategori")}
                            />

                            <h2 className="py-3 px-7 flex items-center uppercase font-extrabold bg-white-light/30 dark:bg-dark dark:bg-opacity-[0.08] -mx-4 mb-1">
                                <svg
                                    className="w-4 h-5 flex-none hidden"
                                    viewBox="0 0 24 24"
                                    stroke="currentColor"
                                    strokeWidth="1.5"
                                    fill="none"
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                >
                                    <line x1="5" y1="12" x2="19" y2="12"></line>
                                </svg>
                                <span>Menu utama</span>
                            </h2>

                            <NavItemSingle
                                label="Dokumen saya"
                                to="/dokumenku"
                                menuPath={["dokumenku"]}
                                currentMenu={currentMenu}
                                buttonSVG={<DocumentsBD
                                    className="group-hover:!text-primary"
                                    width="20"
                                    height="20"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                />}
                                buttonClick={() => toggleMenu("dokumenku")}
                            />

                        </ul>
                    </PerfectScrollbar>
                </div>
            </nav>
        </div>
    );
}

export default LKSide;
