import Portals from '@/Components/Portals';
import LoaderImage from '@/Components/svg/LoaderImage';
import { SidebarControl, UserState } from '@/LabState';
import { PageProps } from '@/types'
import { usePage } from '@inertiajs/react';
import React, { PropsWithChildren, Suspense } from 'react'
import { useRecoilState } from 'recoil';
import LKSide from './LKSide';
import LKHead from './LKHead';
import LKFoot from './LKFoot';

function LKLayout({ pageprops, children }: PropsWithChildren<{ pageprops?: PageProps }>) {
    const [showLoader, setShowLoader] = React.useState(true);
    const [showTopButton, setShowTopButton] = React.useState(false);
    const [sidebar, setsidebar] = useRecoilState(SidebarControl);
    const pp: any = usePage().props;
    let [us, setus] = useRecoilState(UserState);

    const toggleSidebar = () => setsidebar(!sidebar);

    const goToTop = () => {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    };

    const onScrollHandler = () => {
        if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
            setShowTopButton(true);
        } else {
            setShowTopButton(false);
        }
    };

    React.useEffect(() => {
        window.addEventListener('scroll', onScrollHandler);

        const screenLoader = document.getElementsByClassName('screen_loader');
        if (screenLoader?.length) {
            screenLoader[0].classList.add('animate__fadeOut');
            setTimeout(() => {
                setShowLoader(false);
            }, 200);
        }

        return () => {
            window.removeEventListener('onscroll', onScrollHandler);
        };
    }, []);

    return (
        <div className={`${(sidebar && 'toggle-sidebar') || ''} vertical full ltr main-section antialiased relative font-nunito text-sm font-normal`}>
            <div className="relative">
                <div className={`${(!sidebar && 'hidden') || ''} fixed inset-0 bg-[black]/60 z-50 lg:hidden`} onClick={() => toggleSidebar()}></div>
                {showLoader && <LoaderImage />}
                <div className="fixed bottom-6 ltr:right-6 rtl:left-6 z-50">
                    {showTopButton && (
                        <button type="button" className="btn btn-outline-primary rounded-full p-2 animate-pulse bg-[#fafafa] dark:bg-[#060818] dark:hover:bg-primary" onClick={goToTop}>
                            <svg xmlns="http://www.w3.org/2000/svg" className="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth="1.5">
                                <path strokeLinecap="round" strokeLinejoin="round" d="M8 7l4-4m0 0l4 4m-4-4v18" />
                            </svg>
                        </button>
                    )}
                </div>

                <div className={`navbar-floating main-container min-h-screen text-black dark:text-white-dark`}>
                    {/* BEGIN SIDEBAR */}
                    <LKSide />
                    {/* END SIDEBAR */}

                    {/* BEGIN CONTENT AREA */}
                    <div className="main-content">
                        {/* BEGIN TOP NAVBAR */}
                        <LKHead auth={{ user: pp.auth.user }} />
                        {/* END TOP NAVBAR */}
                        <Suspense>
                            <div className={`animate__fadeInLeft p-6 animate__animated`}>
                                {children}

                                {/* BEGIN FOOTER */}
                                <LKFoot />
                                {/* END FOOTER */}
                            </div>
                        </Suspense>
                        <Portals />
                    </div>
                    {/* END CONTENT AREA */}
                </div>
            </div>
        </div>
    )
}

export default LKLayout
