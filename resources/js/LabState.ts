import { atom } from "recoil";
import { User } from "./types";

export const SidebarControl = atom({
    key: "SidebarControl",
    default: false,
});

export const UserState = atom<User>({
    key: "UserState",
    default: undefined,
});
