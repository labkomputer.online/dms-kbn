import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";

/**
 * return value from query url
 * @param name: name of query parameter
 * @returns
 */
export function getQueryString(name: string) {
  const prm = new URLSearchParams(window.location.search);
  return prm.get(name) ? prm.get(name) : null;
}

export function popMessage(msg: string) {
  const toast = withReactContent(Swal)
  toast.fire({
    text: msg,
    toast: true,
    position: 'top-right',
    showConfirmButton: false,
    timer: 3000,
    showCloseButton: true,
  })
}
