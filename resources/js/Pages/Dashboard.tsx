import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout';
import { Head } from '@inertiajs/react';
import { PageProps } from '@/types';
import React from 'react';
import LKLayout from '@/Layouts/Labkom/LKLayout';

function Dashboard({ auth }: PageProps) {
    return (
        <React.Fragment>
            <Head title='Dashboard' />
        </React.Fragment>
    );
}

Dashboard.layout = (page: any) => <LKLayout children={page} />

export default Dashboard
