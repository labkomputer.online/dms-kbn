import Modal from "@/Components/Modal";
import CrossIcon from "@/Components/svg/CrossIcon";
import Penciline from "@/Components/svg/Penciline";
import Trashbin from "@/Components/svg/Trashbin";
import LKLayout from "@/Layouts/Labkom/LKLayout";
import { appurl } from "@/helpers/constant";
import { popMessage } from "@/helpers/functions";
import { Category } from "@/types";
import { InertiaFormProps, useForm } from "@inertiajs/inertia-react";
import { Head, Link, usePage } from "@inertiajs/react";
import { createStyles } from "@mantine/core";
import axios from "axios";
import { DataTable, DataTableSortStatus } from "mantine-datatable";
import React from "react";
import { useQuery } from "react-query";
import Select from "react-select";

const useStyles = createStyles((theme) => ({
    modalHeader: {
        borderBottom: `1px solid ${
            theme.colorScheme === "dark"
                ? theme.colors.dark[5]
                : theme.colors.gray[2]
        }`,
        marginBottom: theme.spacing.md,
    },
    modalTitle: {
        color:
            theme.colorScheme === "dark"
                ? theme.colors.dark[2]
                : theme.colors.gray[8],
        fontWeight: 700,
    },
    modalContent: {
        maxWidth: 300,
    },
    modalLabel: { width: 80 },
}));

function CategoryMain() {
    const pp: any = usePage().props;
    const {
        classes,
        theme: {
            breakpoints: { xs: xsm },
        },
    } = useStyles();
    const aboveXsMediaQuery = `(min-width: ${xsm})`;
    let [page, setPage] = React.useState(1);
    let [sortStatus, setSortStatus] = React.useState<DataTableSortStatus>({
        columnAccessor: "name",
        direction: "asc",
    });
    let [modal, setmodal] = React.useState(false);
    let frm: InertiaFormProps<{
        id: string;
        name: string;
        info: string;
        parentSel: {
            value: string;
            label: string;
        };
    }> = useForm({
        id: "",
        name: "",
        info: "",
        parentSel: {
            value: "",
            label: "Pilih induk kategori",
        },
    });

    const query = useQuery(
        ["category", sortStatus.columnAccessor, sortStatus.direction, page],
        async () => getData(),
        { refetchOnWindowFocus: true }
    );

    const getData = async () => {
        let out: { data: Category[]; total: number } = {
            data: [],
            total: 0,
        };
        return await axios({
            method: "get",
            url: `${appurl}/kategori/lists`,
            data: {},
        })
            .then((res) => {
                if (res.data.status === "success") {
                    out = {
                        data: res.data.data.data,
                        total: res.data.data.total,
                    };
                    return out;
                }
            })
            .catch((res) => {
                popMessage("Gagal mengambil data");
                return out;
            });
    };

    const showNotif = () => {
        if (pp.flash.message) {
            // setfrmodal(false)
            // frm.reset()
            popMessage(pp.flash.message);
        }
    };

    const handleEditClick = (c: Category) => {
        frm.setData({
            id: c.id as string,
            name: c.name as string,
            info: c.info as string,
            parentSel: c.parent ? {
                label: c.catparent?.name as string,
                value: c.catparent?.id as string
            } : {
                value: "",
                label: "Pilih induk kategori"
            }
        });
        setmodal(true);
    }

    const postData = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        frm.post(route('category.post'));
    }

    const handleModalClose = () => {
        frm.reset();
        setmodal(false);
    }

    const KategoriOpt = () => {
        const res: Category[] = pp?.kategori;
        let out: { value: string; label: string }[] = [
            {
                value: "",
                label: "Pilih induk kategori",
            },
        ];
        res.forEach((v) => {
            out.push({
                value: v.id as string,
                label: v.name as string
            })
        });
        return out;
    }

    React.useEffect(() => {
        console.log(pp)
        if (pp?.flash?.status) {
            showNotif();
            query.refetch();
            frm.reset();
        }
    }, [pp.flash]);

    return (
        <React.Fragment>
            <Head title="Kategori" />
            <div className="space-y-6">
                <div className="panel">
                    <div className="flex items-center justify-between mb-5">
                        <h5 className="font-semibold text-lg dark:text-white-light">
                            Kategori
                        </h5>
                        <button
                            type="button"
                            className="btn btn-success flex"
                            onClick={() => {
                                frm.reset();
                                setmodal(true);
                            }}
                        >
                            Tambah
                        </button>
                    </div>

                    <DataTable
                        height={400}
                        withBorder
                        borderRadius="sm"
                        withColumnBorders
                        striped
                        verticalAlignment="top"
                        fetching={query.isFetching}
                        records={query.data?.data}
                        key="id"
                        page={page}
                        onPageChange={setPage}
                        totalRecords={query.data?.total}
                        recordsPerPage={10}
                        columns={[
                            {
                                accessor: "name",
                                title: "Kategori",
                                sortable: false,
                            },
                            {
                                accessor: "info",
                                title: "Info",
                                sortable: false,
                            },
                            {
                                accessor: "catparent.name",
                                title: "Induk Kategori",
                                sortable: false,
                            },
                            {
                                accessor: "id",
                                title: "#",
                                sortable: false,
                                render: (r) => {
                                    return (
                                        <div className="flex items-center justify-center">
                                            <div>
                                                <button
                                                    type="button"
                                                    className="text-warning"
                                                    title="Ubah kategori"
                                                    onClick={() => handleEditClick(r)}
                                                >
                                                    <Penciline
                                                        className={`ltr:mr-2 rtl:ml-2`}
                                                        width="20"
                                                        height="20"
                                                        viewBox="0 0 24 24"
                                                        fill="none"
                                                        xmlns="http://www.w3.org/2000/svg"
                                                    />
                                                </button>
                                            </div>
                                            <div>
                                                <Link
                                                    className="text-danger"
                                                    method="delete"
                                                    as="button"
                                                    title="Hapus kategori"
                                                    onBefore={() =>
                                                        window.confirm(
                                                            "Yakin ingin menghapus?"
                                                        )
                                                    }
                                                    href=""
                                                >
                                                    <Trashbin
                                                        className="ltr:mr-2 rtl:ml-2"
                                                        width="20"
                                                        height="20"
                                                        viewBox="0 0 24 24"
                                                        fill="none"
                                                        xmlns="http://www.w3.org/2000/svg"
                                                    />
                                                </Link>
                                            </div>
                                        </div>
                                    );
                                },
                            },
                        ]}
                    />
                </div>
            </div>

            <Modal
                show={modal}
                maxWidth="md"
                onClose={() => handleModalClose()}
                contentOverflowY="visible"
            >
                <div className="flex items-center justify-between bg-[#fbfbfb] px-5 py-3 dark:bg-[#121c2c]">
                    <div className="text-lg font-bold">Kategori</div>
                    <button
                        type="button"
                        className="text-white-dark hover:text-dark"
                        onClick={() => handleModalClose()}
                    >
                        <CrossIcon
                            xmlns="http://www.w3.org/2000/svg"
                            width="20"
                            height="20"
                            viewBox="0 0 24 24"
                            fill="none"
                            stroke="currentColor"
                            strokeWidth="1.5"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                        />
                    </button>
                </div>
                <form onSubmit={postData}>
                    <div className="px-5 pb-3">
                        <input type="hidden" value={frm.data.id} />
                        <div className="mb-3">
                            <label htmlFor="name">Kategori</label>
                            <input
                                id="name"
                                name="name"
                                type="text"
                                className="form-input"
                                value={frm.data.name}
                                onChange={(e) => {
                                    frm.setData("name", e.target.value);
                                }}
                            />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="info">Info</label>
                            <input
                                id="info"
                                name="info"
                                type="text"
                                className="form-input"
                                value={frm.data.info}
                                onChange={(e) => {
                                    frm.setData("info", e.target.value);
                                }}
                            />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="departemen">
                                Kategori induk
                            </label>
                            <Select
                                isSearchable={true}
                                id="departemen"
                                name="departemen"
                                defaultValue={KategoriOpt()[0]}
                                value={frm.data.parentSel}
                                options={KategoriOpt()}
                                inputId="departemen"
                                onChange={(e) => {
                                    frm.setData("parentSel", {
                                        value: e?.value as string,
                                        label: e?.label as string,
                                    });
                                }}
                            />
                        </div>
                        <div className="mt-8 flex items-center justify-end">
                            <button
                                type="submit"
                                disabled={frm.processing}
                                className="btn btn-primary ltr:ml-4 rtl:mr-4"
                            >
                                Simpan
                            </button>
                        </div>
                    </div>
                </form>
            </Modal>
        </React.Fragment>
    );
}

CategoryMain.layout = (page: any) => <LKLayout children={page} />;

export default CategoryMain;
