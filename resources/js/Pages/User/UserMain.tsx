import Modal from "@/Components/Modal";
import CrossIcon from "@/Components/svg/CrossIcon";
import Penciline from "@/Components/svg/Penciline";
import Trashbin from "@/Components/svg/Trashbin";
import LKLayout from "@/Layouts/Labkom/LKLayout";
import { appurl } from "@/helpers/constant";
import { popMessage } from "@/helpers/functions";
import { Departemen, User } from "@/types";
import { InertiaFormProps, useForm } from "@inertiajs/inertia-react";
import { Head, Link, usePage, router } from "@inertiajs/react";
import { createStyles } from "@mantine/core";
import axios from "axios";
import { DataTable, DataTableSortStatus } from "mantine-datatable";
import React from "react";
import { useQuery } from "react-query";
import Select from "react-select";

const useStyles = createStyles((theme) => ({
    modalHeader: {
        borderBottom: `1px solid ${
            theme.colorScheme === "dark"
                ? theme.colors.dark[5]
                : theme.colors.gray[2]
        }`,
        marginBottom: theme.spacing.md,
    },
    modalTitle: {
        color:
            theme.colorScheme === "dark"
                ? theme.colors.dark[2]
                : theme.colors.gray[8],
        fontWeight: 700,
    },
    modalContent: {
        maxWidth: 300,
    },
    modalLabel: { width: 80 },
}));

function UserMain() {
    const pp: any = usePage().props;
    const optRole = [
        {
            value: "admin",
            label: "Administrator",
        },
        {
            value: "employee",
            label: "Pegawai",
        },
    ];

    const optJab = [
        {
            value: "Anggota",
            label: "Anggota",
        },
        {
            value: "PIC",
            label: "PIC",
        },
    ];

    const {
        classes,
        theme: {
            breakpoints: { xs: xsm },
        },
    } = useStyles();
    const aboveXsMediaQuery = `(min-width: ${xsm})`;
    let [page, setPage] = React.useState(1);
    let [sortStatus, setSortStatus] = React.useState<DataTableSortStatus>({
        columnAccessor: "name",
        direction: "asc",
    });
    let [modal, setmodal] = React.useState(false);
    let frm: InertiaFormProps<{
        id: string;
        name: string;
        email: string;
        pswd: string;
        role?: {
            value: string;
            label?: string;
        };
        departemen?: {
            value: string;
            label?: string;
        };
        jabatan?: {
            value: string;
            label?: string;
        };
    }> = useForm({
        id: "",
        name: "",
        email: "",
        pswd: "",
    });

    const submitUser = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        frm.post(`${appurl}/user/exec`);
    };

    const getData = async () => {
        let out: { data: User[]; total: number } = {
            data: [],
            total: 0,
        };
        return await axios({
            method: "get",
            url: `${appurl}/user/lists/${pp.type}`,
            data: {},
        })
            .then((res) => {
                out = {
                    data: res.data.data.data,
                    total: res.data.data.total,
                };
                return out;
            })
            .catch((res) => {
                // console.log(res);
                popMessage("Gagal mengambil data");
                return out;
            });
    };

    const query = useQuery(
        ["employee", sortStatus.columnAccessor, sortStatus.direction, page],
        async () => getData(),
        { refetchOnWindowFocus: true }
    );

    const dataDepartemen = () => {
        const dep: Departemen[] = pp?.departemen;
        let out: { value: string; label: string }[] = [
            {
                value: "",
                label: "Pilih departemen",
            },
        ];
        dep.forEach((v) => {
            out.push({
                value: v.id as string,
                label: v.name as string,
            });
        });

        return out;
    };

    const showNotif = () => {
        if (pp.flash.message) {
            // setfrmodal(false)
            // frm.reset()
            popMessage(pp.flash.message);
        }
    };

    const kolomDepartemen = () => {
        if (pp.type === "pegawai") {
            return [
                {
                    accessor: "departemennya.name",
                    title: "Departemen",
                    sortable: false,
                },
                {
                    accessor: "jabatandep",
                    title: "Jabatan",
                    sortable: false,
                },
            ];
        }
        return [];
    };

    React.useEffect(() => {
        if (pp?.flash?.status) {
            showNotif();
            query.refetch();
            frm.reset();
        }
    }, [pp.flash]);

    return (
        <React.Fragment>
            <Head title="Data pengguna" />
            <div className="space-y-6">
                <div className="panel">
                    <div className="flex items-center justify-between mb-5">
                        <h5 className="font-semibold text-lg dark:text-white-light">
                            Pengguna
                        </h5>
                        <button
                            type="button"
                            className="btn btn-success flex"
                            onClick={() => {
                                // frm.reset();
                                setmodal(true);
                            }}
                        >
                            Tambah
                        </button>
                    </div>

                    <div className="datatables">
                        <DataTable
                            height={400}
                            withBorder
                            borderRadius="sm"
                            withColumnBorders
                            striped
                            verticalAlignment="top"
                            fetching={query.isFetching}
                            records={query.data?.data}
                            key="id"
                            page={page}
                            onPageChange={setPage}
                            totalRecords={query.data?.total}
                            recordsPerPage={10}
                            columns={[
                                {
                                    accessor: "email",
                                    title: "Email pengguna",
                                    sortable: false,
                                },
                                {
                                    accessor: "name",
                                    title: "Nama pengguna",
                                    sortable: false,
                                },
                                {
                                    accessor: "role",
                                    title: "Role pengguna",
                                    sortable: false,
                                },
                                {
                                    accessor: "departemennya.name",
                                    title: "Departemen",
                                    sortable: false,
                                    hidden: pp.type !== "pegawai" ?? true,
                                },
                                {
                                    accessor: "jabatandep",
                                    title: "Jabatan",
                                    sortable: false,
                                    hidden: pp.type !== "pegawai" ?? true,
                                },
                                {
                                    accessor: "id",
                                    title: "#",
                                    width: 150,
                                    sortable: false,
                                    render: (r) => {
                                        return r.role === "root" ? (
                                            <></>
                                        ) : (
                                            <div className="flex items-center justify-center">
                                                <div>
                                                    <button
                                                        type="button"
                                                        className="text-warning"
                                                        title="Ubah operator"
                                                        onClick={() => {
                                                            frm.setData({
                                                                id: r.id,
                                                                email: r.email,
                                                                name: r.name,
                                                                pswd: "",
                                                                role: {
                                                                    value: r.role as string,
                                                                },
                                                                departemen: {
                                                                    value: r.departemen as string,
                                                                },
                                                                jabatan: {
                                                                    value: r.jabatandep as string,
                                                                },
                                                            });
                                                            setmodal(true);
                                                        }}
                                                    >
                                                        <Penciline
                                                            className={`ltr:mr-2 rtl:ml-2`}
                                                            width="20"
                                                            height="20"
                                                            viewBox="0 0 24 24"
                                                            fill="none"
                                                            xmlns="http://www.w3.org/2000/svg"
                                                        />
                                                    </button>
                                                </div>
                                                <div>
                                                    <Link
                                                        className="text-danger"
                                                        method="delete"
                                                        as="button"
                                                        href={route(
                                                            "user.remove",
                                                            {
                                                                id: r.id,
                                                                type: r.role,
                                                            }
                                                        )}
                                                        onBefore={() =>
                                                            window.confirm(
                                                                "Yakin ingin hapus data ini?"
                                                            )
                                                        }
                                                        title="Hapus pegawai"
                                                    >
                                                        <Trashbin
                                                            className="ltr:mr-2 rtl:ml-2"
                                                            width="20"
                                                            height="20"
                                                            viewBox="0 0 24 24"
                                                            fill="none"
                                                            xmlns="http://www.w3.org/2000/svg"
                                                        />
                                                    </Link>
                                                </div>
                                            </div>
                                        );
                                    },
                                },
                            ]}
                        />
                    </div>
                </div>
            </div>

            <Modal
                show={modal}
                maxWidth="md"
                onClose={() => {
                    frm.reset();
                    setmodal(false);
                }}
            >
                <div className="flex items-center justify-between bg-[#fbfbfb] px-5 py-3 dark:bg-[#121c2c]">
                    <div className="text-lg font-bold">Pengguna</div>
                    <button
                        type="button"
                        className="text-white-dark hover:text-dark"
                        onClick={() => setmodal(false)}
                    >
                        <CrossIcon
                            xmlns="http://www.w3.org/2000/svg"
                            width="20"
                            height="20"
                            viewBox="0 0 24 24"
                            fill="none"
                            stroke="currentColor"
                            strokeWidth="1.5"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                        />
                    </button>
                </div>
                <div className="px-5 pb-3">
                    <form onSubmit={submitUser}>
                        <input type="hidden" value={frm.data.id} />
                        <div className="mb-3">
                            <label htmlFor="role">Role Pegawai</label>
                            <Select
                                isSearchable={true}
                                id="role"
                                name="role"
                                defaultValue={optRole[0]}
                                value={frm.data.role}
                                options={optRole}
                                inputId="role"
                                onChange={(e) => {
                                    frm.setData("role", {
                                        value: e?.value as string,
                                        label: e?.label as string,
                                    });
                                }}
                            />
                        </div>

                        {frm.data.role?.value === "employee" ? (
                            <React.Fragment>
                                <div className="mb-3">
                                    <label htmlFor="departemen">
                                        Departemen
                                    </label>
                                    <Select
                                        isSearchable={true}
                                        id="departemen"
                                        name="departemen"
                                        defaultValue={dataDepartemen()[0]}
                                        value={frm.data.departemen}
                                        options={dataDepartemen()}
                                        inputId="departemen"
                                        onChange={(e) => {
                                            frm.setData("departemen", {
                                                value: e?.value as string,
                                                label: e?.label as string,
                                            });
                                        }}
                                    />
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="jabatan">Jabatan</label>
                                    <Select
                                        isSearchable={true}
                                        id="jabatan"
                                        name="jabatan"
                                        defaultValue={optJab[0]}
                                        value={frm.data.jabatan}
                                        options={optJab}
                                        inputId="jabatan"
                                        onChange={(e) => {
                                            frm.setData("jabatan", {
                                                value: e?.value as string,
                                                label: e?.label as string,
                                            });
                                        }}
                                    />
                                </div>
                            </React.Fragment>
                        ) : (
                            <></>
                        )}

                        <div className="mb-3">
                            <label htmlFor="name">Nama Pegawai</label>
                            <input
                                id="name"
                                name="name"
                                type="text"
                                className="form-input"
                                value={frm.data.name}
                                onChange={(e) => {
                                    frm.setData("name", e.target.value);
                                }}
                            />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="email">Email Pegawai</label>
                            <input
                                id="email"
                                name="email"
                                type="email"
                                className="form-input"
                                value={frm.data.email}
                                onChange={(e) => {
                                    frm.setData("email", e.target.value);
                                }}
                            />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="name">Password Pegawai</label>
                            <input
                                id="pswd"
                                name="pswd"
                                type="password"
                                className="form-input"
                                value={frm.data.pswd}
                                onChange={(e) => {
                                    frm.setData("pswd", e.target.value);
                                }}
                            />
                        </div>
                        <div className="mt-8 flex items-center justify-end">
                            <button
                                type="submit"
                                disabled={frm.processing}
                                className="btn btn-primary ltr:ml-4 rtl:mr-4"
                            >
                                Simpan
                            </button>
                        </div>
                    </form>
                </div>
            </Modal>
        </React.Fragment>
    );
}

UserMain.layout = (page: any) => <LKLayout children={page} />;

export default UserMain;
