import { Head, Link, useForm } from '@inertiajs/react';
import React, { useEffect } from 'react';

export default function Login (props: { status: any, canResetPassword: any }) {
  const { data, setData, post, processing, errors, reset } = useForm({
    email: '',
    password: '',
    remember: false,
  });

  useEffect(() => {
      return () => {
          reset('password');
      };
  }, []);

  const submit = (e: any) => {
      e.preventDefault();

      post(route('login'));
  };

  const submitForm = () => {
      //navigate('/');
  };

  return (
    <React.Fragment>
      <Head title='Login' />

      {props.status && <div className="mb-4 font-medium text-sm text-green-600">{props.status}</div>}

      <div className="flex justify-center items-center min-h-screen bg-cover bg-center bg-[url('/assets/images/map.svg')] dark:bg-[url('/assets/images/map-dark.svg')]">
        <div className="panel sm:w-[480px] m-6 max-w-lg w-full">
          <h2 className="font-bold text-2xl mb-3">Sign In</h2>
          <p className="mb-7">Enter your email and password to login</p>
          <form className="space-y-5" onSubmit={submit}>
            <div>
                <label htmlFor="email">Email</label>
                <input id="email" type="email" name="email" value={data.email} autoComplete="username" onChange={(e) => setData('email', e.target.value)} className="form-input" placeholder="Enter Email" />
                <sub className="text-red-600">{errors ? errors.email : ''}</sub>
            </div>
            <div>
                <label htmlFor="password">Password</label>
                <input id="password" type="password" name="password" value={data.password} autoComplete="current-password" onChange={(e) => setData('password', e.target.value)} className="form-input" placeholder="Enter Password" />
                <sub>{errors ? errors.password : ''}</sub>
            </div>

            <div>
              <Link href={route('password.request')} className="underline text-sm text-gray-600 dark:text-gray-400 hover:text-gray-900 dark:hover:text-gray-100 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 dark:focus:ring-offset-gray-800">
                Lupa password?
              </Link>
            </div>

            <button type="submit" className="btn btn-primary w-full">
                SIGN IN
            </button>
          </form>

        </div>
      </div>
    </React.Fragment>
  );
};
