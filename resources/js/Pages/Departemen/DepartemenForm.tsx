import LKLayout from "@/Layouts/Labkom/LKLayout";
import { appurl } from "@/helpers/constant";
import { popMessage } from "@/helpers/functions";
import { Departemen, PageProps, User } from "@/types";
import { useForm } from "@inertiajs/inertia-react";
import { Head, usePage } from "@inertiajs/react";
import axios from "axios";
import React from "react";
import Select from "react-select";

function DepartemenForm(pageprops: PageProps) {
    const pp: any = usePage().props;
    const departemen: Departemen = pp.departemen;
    const jabatan = [
        {
            value: "Anggota",
            label: "Anggota",
        },
        {
            value: "PIC",
            label: "PIC",
        }
    ];

    let [anggota, setAnggota] = React.useState<User[]>([])

    let frm = useForm({
        id: "",
        name: "",
        info: ""
    });

    const getAnggota = async () => {
        await axios({
            method: "get",
            url: `${appurl}/member/${departemen.id}`
        })
        .then(res => {
            if (res.data.status === 'success') {
                setAnggota(res.data.data);
            }
        })
        .catch(res => {
            popMessage("Gagal mengambil data anggota");
        })
    }

    const ubahJabatan = async (userId: string, tipe: string) => {
        await axios({
            method: "get",
            url: `${appurl}/member/${userId}/${tipe}`
        })
        .then(res => {
            if (res.data.status === 'success') {
                popMessage("Jabatan anggota berhasil diubah")
            }
        })
        .catch(res => {
            popMessage('Gagal memperbarui data');
        })
    }

    React.useEffect(() => {
        // console.log(pp)
        if (departemen.id) {
            getAnggota();
            frm.setData({
                id: departemen.id,
                name: departemen.name as string,
                info: departemen.info as string
            })
        }
    }, [])

    const postForm = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        frm.post(`${appurl}/departemen/post`);
    }

    return (
        <React.Fragment>
            <Head title="Form Departemen" />
            <div className="space-y-6">
                <div className="flex w-full align-middle justify-between">
                    <h5 className="font-semibold text-lg dark:text-white-light">
                        Departemen
                    </h5>
                </div>

                <div className="grid grid-cols-1 md:grid-cols-3 gap-4 mt-4">
                    <form onSubmit={postForm}>
                        <input type="hidden" name="id" value={frm.data.id} />
                        <div className="panel">
                            <div className="flex items-center justify-between mb-5">
                                <h5 className="font-semibold text-lg dark:text-white-light">
                                    Informasi
                                </h5>
                                <button
                                    type="submit"
                                    className="btn btn-success"
                                >
                                    Simpan
                                </button>
                            </div>
                            <div className="space-y-6">
                                <div>
                                    <label htmlFor="name">
                                        Nama Departemen
                                    </label>
                                    <input name="name" className="form-input" value={frm.data.name} onChange={(e) => frm.setData("name", e.target.value)} />
                                </div>
                                <div>
                                    <label htmlFor="info">
                                        Informai Departemen
                                    </label>
                                    <textarea
                                        name="info"
                                        className="form-textarea"
                                        rows={4}
                                        value={frm.data.info}
                                        onChange={(e) => frm.setData("info", e.target.value)}
                                    />
                                </div>
                            </div>
                        </div>
                    </form>

                    <div className="panel col-span-2">
                        <div className="flex items-center justify-between mb-5">
                            <h5 className="font-semibold text-lg dark:text-white-light">
                                Anggota
                            </h5>
                        </div>

                        <div className="space-y-6">
                            <div className="table-responsive mb-5">
                                <table>
                                    <thead>
                                        <tr>
                                            <th>Nama</th>
                                            <th>Jabatan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            anggota.map((v, i) => {
                                                return (
                                                    <tr key={i}>
                                                        <td>{v.name}</td>
                                                        <td>
                                                            <select className="form-select" onChange={(e) => {
                                                                ubahJabatan(v.id, e.target.value)
                                                            }}>
                                                                <option value="PIC" selected={v.jabatandep === "PIC" ?? true}>PIC</option>
                                                                <option value="Anggota" selected={v.jabatandep === "Anggota" ?? true}>Anggota</option>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                )
                                            })
                                        }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
}

DepartemenForm.layout = (page: any) => <LKLayout children={page} />;

export default DepartemenForm;
