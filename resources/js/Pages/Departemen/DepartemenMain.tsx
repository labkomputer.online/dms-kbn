import Penciline from "@/Components/svg/Penciline";
import Trashbin from "@/Components/svg/Trashbin";
import LKLayout from "@/Layouts/Labkom/LKLayout";
import { appurl } from "@/helpers/constant";
import { popMessage } from "@/helpers/functions";
import { Departemen } from "@/types";
import { Head, Link, usePage } from "@inertiajs/react";
import { createStyles } from "@mantine/core";
import axios from "axios";
import { DataTable, DataTableSortStatus } from "mantine-datatable";
import React from "react";
import { useQuery } from "react-query";

const useStyles = createStyles((theme) => ({
    modalHeader: {
        borderBottom: `1px solid ${
            theme.colorScheme === "dark"
                ? theme.colors.dark[5]
                : theme.colors.gray[2]
        }`,
        marginBottom: theme.spacing.md,
    },
    modalTitle: {
        color:
            theme.colorScheme === "dark"
                ? theme.colors.dark[2]
                : theme.colors.gray[8],
        fontWeight: 700,
    },
    modalContent: {
        maxWidth: 300,
    },
    modalLabel: { width: 80 },
}));

function DepartemenMain() {
    const pp: any = usePage().props;
    const {
        classes,
        theme: {
            breakpoints: { xs: xsm },
        },
    } = useStyles();
    const aboveXsMediaQuery = `(min-width: ${xsm})`;
    let [page, setPage] = React.useState(1);
    let [sortStatus, setSortStatus] = React.useState<DataTableSortStatus>({
        columnAccessor: "name",
        direction: "asc",
    });

    const getData = async () => {
        let out: {data: Departemen[], total: number} = {
            data: [],
            total: 0
        }
        return await axios({
            method: "get",
            url: `${appurl}/departemen/lists`,
            data: {}
        })
        .then(res => {
            if (res.data.status === "success") {
                out = {
                    data: res.data.data.data,
                    total: res.data.data.total,
                };
                return out;
            }
        })
        .catch(res => {
            popMessage("Gagal mengambil data");
            return out;
        })
    }

    const query = useQuery(
        ["departemen", sortStatus.columnAccessor, sortStatus.direction, page],
        async () => getData(),
        { refetchOnWindowFocus: true }
    );

    const showNotif = () => {
        if (pp?.flash?.message) {
            popMessage(pp?.flash?.message);
        }
    }

    React.useEffect(() => {
        if (pp?.flash?.status) {
            // console.log(pp?.flash?.status)
            showNotif()
            query.refetch();
        }
    }, [pp.flash]);

    return (
        <React.Fragment>
            <Head title="Departemen" />
            <div className="space-y-6">
                <div className="panel">
                    <div className="flex items-center justify-between mb-5">
                        <h5 className="font-semibold text-lg dark:text-white-light">
                            Departemen
                        </h5>
                        <Link href="/departemen/form" className="btn btn-success flex">
                            Tambah
                        </Link>
                    </div>

                    <div className="datatables">
                        <DataTable
                            height={400}
                            withBorder
                            borderRadius="sm"
                            withColumnBorders
                            striped
                            verticalAlignment="top"
                            fetching={query.isFetching}
                            records={query.data?.data}
                            key="id"
                            page={page}
                            onPageChange={setPage}
                            totalRecords={query.data?.total}
                            recordsPerPage={10}
                            columns={[
                                {
                                    accessor: "name",
                                    title: "Departemen",
                                    sortable: false,
                                },
                                {
                                    accessor: "info",
                                    title: "Info",
                                    sortable: false,
                                },
                                {
                                    accessor: 'id',
                                    title: '#',
                                    width: 150,
                                    sortable: false,
                                    render: (r) => {
                                        return (
                                            <div className='flex items-center justify-center'>
                                                <div>
                                                    <Link href={route('departemen.form', {id: r.id})} className='text-warning' title='Ubah operator'>
                                                        <Penciline className={`ltr:mr-2 rtl:ml-2`} width="20" height="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" />
                                                    </Link>
                                                </div>
                                                <div>
                                                    <Link className="text-danger" method='delete' as="button" href={route('departemen.remove', {id: r.id})} onBefore={() => window.confirm('Yakin ingin hapus data ini?')} title="Hapus departemen">
                                                        <Trashbin className="ltr:mr-2 rtl:ml-2" width="20" height="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" />
                                                    </Link>
                                                </div>
                                            </div>
                                        )
                                    }
                                }
                            ]}
                        />
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
}

DepartemenMain.layout = (page: any) => <LKLayout children={page} />;

export default DepartemenMain;
