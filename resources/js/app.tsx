import './bootstrap';
// import '../css/app.css';

// Tailwind css
import '../css/tailwind.css';

import { createRoot, hydrateRoot } from 'react-dom/client';
import { createInertiaApp } from '@inertiajs/react';
import { resolvePageComponent } from 'laravel-vite-plugin/inertia-helpers';
import LKLayout from './Layouts/Labkom/LKLayout';
import { QueryClient, QueryClientProvider } from 'react-query';
import { RecoilRoot } from 'recoil';
import { BrowserRouter } from 'react-router-dom';
import { MantineProvider } from '@mantine/core';

const appName = import.meta.env.VITE_APP_NAME || 'Laravel';

const qc = new QueryClient();

createInertiaApp({
    title: (title) => `${title} - ${appName}`,
    resolve: (name) => {
        // resolvePageComponent(`./Pages/${name}.tsx`, import.meta.glob('./Pages/**/*.tsx'))
        const pages = import.meta.glob('./Pages/**/*.tsx', { eager: true });
        let page: any = pages[`./Pages/${name}.tsx`];
        page.default.layout = page.default.layout && ((page: any) => <LKLayout children={page} />)
        return page
    },
    setup({ el, App, props }) {
        const root = createRoot(el);

        root.render(
            <QueryClientProvider client={qc}>
                <RecoilRoot>
                    <MantineProvider>
                        <BrowserRouter>
                            <App {...props} />
                        </BrowserRouter>
                    </MantineProvider>
                </RecoilRoot>
            </QueryClientProvider>
        );
    },
    progress: {
        color: '#4B5563',
    },
});
